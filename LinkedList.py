#!/usr/bin/env python
# coding: utf-8

# In[1]:

class Node:
    
    def __init__(self, data=None, next=None, head=None):
        self.data = data
        self.next = next
        self.head = head
    
    #insert new element at the beginning
    def push(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
    
    #insert new element at the end
    def append(self, data):
        if not self.head:
            self.head = Node(data)
            return
        node = self.head
        while node.next:
            node = node.next
        node.next = Node(data)
    
    #insert new element right after a given element (target method of the assignment)
    def insert(self, element, data): 
        e = self.head
        while e is not None:
            if e.data == element:
                break
            e = e.next
        if e is None:
            print("Item not found in the list.")
        else:
            node = Node(data)
            node.next = e.next
            e.next = node
    
    #return the element at a given index
    def at(self, index):
        count = 0
        node = self.head
        while node:
            if count == index:
                return node.data
            count += 1
            node = node.next
    
    #insert new element right after a given index
    def insert_after_index(self, index, data):
        if index == 0:
            node = Node(data)
            node.next = self.head
            self.head = node
        i = 0
        e = self.head
        while i < index and e is not None:
            e = e.next
            i = i + 1
        if e is None:
            print("Index out of range.")
        else: 
            node = Node(data)
            node.next = e.next
            e.next = node
            
    #print out the linked list       
    def print_nodes(self):
        if not self.head:
            print(self.head)
        node = self.head
        while node:
            end = " -> " if node.next else "\n"
            print(node.data, end=end)
            node = node.next


# In[2]:

##Test the class and methods
list_1 = Node()

list_1.head = Node("Mon")
list_1.print_nodes()

list_1.head.next = Node("Tue")
list_1.print_nodes()

list_1.append("Fri")
list_1.print_nodes()

list_1.push("Sun")
list_1.print_nodes()

list_1.insert("Tue", "Wed")
list_1.print_nodes()

list_1.insert("Wes", "Sat")

print(list_1.at(3))
print(list_1.at(10))

list_1.insert_after_index(3, "Thu")
list_1.print_nodes()

list_1.insert_after_index(9, "Thu")

list_1.insert("Fri", "Sat")
list_1.print_nodes()


# In[3]:

##Test time complexity
import time
samples = [100000, 200000, 400000, 800000, 1600000, 3200000]

list_2 = Node()
list_2.head = Node("#")
results = []

for i in samples:
    start = time.time()
    for j in range(i):
        list_2.insert("#", str(j))    
    end = time.time()
    results.append((end-start)/i)
    
print("time = ", results)

import matplotlib.pyplot as plt
plt.plot(samples, results)
plt.show()


###Time Complexity: Constant Time(O(1))